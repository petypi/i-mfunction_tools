'''This decorator takes a function and returns a wrapped version of the same function
that implements the caching logic (memoized_func).
This isPrime function will serve as an  an “expensive” computation'''
import timeit
from check_is_prime import isPrime
def memoize(func):
    cache = dict()

    def memoized_func(*args):
        if args in cache:
            return cache[args]
        result = func(*args)
        cache[args] = result
        return result

    return memoized_func

# Driver Code
#we can speed it up by leveraging the function result caching provided by our memoization decorator:
if __name__ == '__main__':
    #

    memoized_check_prime = memoize(isPrime(13))
    print(timeit.timeit('isPrime(14)', globals=globals(), number=1))